import os
from sys import platform
import sys
import logging
import subprocess
import speedtest
import arrow
import time
logger = logging.getLogger(__name__)
script_location = os.path.abspath(os.path.dirname(__file__))
output_path = os.path.join(script_location, "./speedtest_log.csv")
log_path = os.path.join(script_location, "./speedtest_debug.log")
logging.basicConfig(filename=log_path, level=logging.DEBUG, filemode='a', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(handler)
logging.debug("Starting")
def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

sys.excepthook = handle_exception
# Change these lines!
TESTER = "Justin"
DEVICE_TYPE = "Laptop"
DEVICE = "Dell Inspiron 15 5000"

# Get the access point id on Windows, Mac, and Linux
if platform == "linux" or platform == "linux2":
    bssid = subprocess.run(["iwgetid", "-ar"], check=True,
                       capture_output=True).stdout.decode("utf-8").replace("\n","")
elif platform == "darwin":
    ps = subprocess.Popen(('/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport', '-I'), stdout=subprocess.PIPE)
    bssid = subprocess.check_output(('awk', '/ SSID/ {print substr($0, index($0, $2))}'), stdin=ps.stdout).decode("utf-8")
elif platform == "win32":
    bssid = subprocess.run(["netsh", "wlan", "show", "interfaces"], check=True, capture_output=True).stdout.decode("utf-8").replace("\n","")
logging.debug("BSSID: %s",bssid)
# Set up the test
threads = None
s = speedtest.Speedtest()
count = 0
while (len(s.servers) == 0 and count < 1000):
    if(count>0):
        time.sleep(1)
    logging.debug("Looking for servers...")
    logging.debug("Attempts: %s", count)
    s.get_servers()
    count+=1
s.get_best_server()

# Run the test
print("Testing")
s.download(threads=threads)
s.upload(threads=threads)

# Get the results
download = int(s.results.dict()['download'])/1000000
logging.debug("Down: %s",download)
upload = int(s.results.dict()['upload'])/1000000
logging.debug("Upload: %s",upload)
ping = s.results.dict()['ping']
logging.debug("Ping: %s",ping)

# Record the time
date = arrow.now().format("MM/DD/YY")
logging.debug("Date: %s",date)
time = arrow.now().format("HH:mm")
logging.debug("Time: %s",time)

# Get current file location

logging.debug("Output Path: %s",output_path)

# Save to file
try:
    logging.debug("Appending")
    with open(output_path, "a+") as file:
        file.write(
            f"{date},{time},{TESTER},{bssid},{download},{upload},{ping},{DEVICE_TYPE},{DEVICE}\n")
except(FileNotFoundError):
    logging.debug("Creating")
    with open(output_path, "w") as file:
        file.write("date,time,tester,bssid,download,upload,device_type,device\n")
        file.write(
            f"{date},{time},{TESTER},{bssid},{download},{upload},{ping},{DEVICE_TYPE},{DEVICE}\n")
logging.debug("Done")