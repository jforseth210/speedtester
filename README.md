# Speedtester
_A way of automatically recording network speed tests using speedtest cli_ 

This script was created to periodically perform network speed tests on my college network to evaluate which buildings have the best internet. Script was ran every ten minutes using cron (or Windows Task Scheduler) and results are appened to a csv file for easy analysis. 

## Installation and Usage: 
1. Clone this repo
2. Install the requirements `pip install -r requirements.txt`, preferably in a virtual environment. 
3. Run it using `python speedtester.py`
4. Schedule it to run at regular intervals using your operating system's task scheduleing tools. 

## To get location information: 
Use an app like [https://play.google.com/store/apps/details?id=com.vrem.wifianalyzer](https://play.google.com/store/apps/details?id=com.vrem.wifianalyzer) to scan for access points in all of the locations in question and compare those the the BSSIDs recorded by this script in order to determine location based on connected AP.
